import { createTreeWithEmptyWorkspace } from '@nx/devkit/testing';
import { Tree, readProjectConfiguration, writeJsonFile } from '@nrwl/devkit';
import * as fs from 'fs/promises';
import * as toml from 'toml';

import libgenerator from './libgenerator';
import appgenerator from './appgenerator';
import { NxPythGeneratorSchema } from './schema';


describe('nx-py lib generator', () => {
  let appTree: Tree;
  const options: NxPythGeneratorSchema = { name: 'test' };

  beforeEach(() => {
    appTree = createTreeWithEmptyWorkspace({layout: "apps-libs"});
    writeJsonFile("dist/package.json",{name: "@dummy/source"})
    appTree.root = "dist"
  });

  afterEach(async () => {
    await fs.unlink("dist/package.json")
  });

  it('should create a new lib', async () => {
    appTree.root = `${__dirname}/testdata`
    await libgenerator(appTree, options);
    const config = readProjectConfiguration(appTree, 'test');
    expect(config).toBeDefined();
  });

  it('should update poetry', async () => {
    appTree.root = `${__dirname}/testdata`
    await libgenerator(appTree, options);
    const data = appTree.read('pyproject.toml')
    const tomlParsed = toml.parse(data.toString())
    expect(tomlParsed['tool']['poetry']['packages']).toEqual(expect.arrayContaining([{"from": "libs/test/src/", "include": "test"}])
      )
  })
});

describe('nx-py app generator', () => {
  let appTree: Tree;
  const options: NxPythGeneratorSchema = { name: 'test' };

  beforeEach(() => {
    appTree = createTreeWithEmptyWorkspace();
    writeJsonFile("dist/package.json",{name: "dummy"})
    appTree.root = "dist"
  });

  afterEach(async () => {
    await fs.unlink("dist/package.json")
  });

  it('should create a new app', async () => {
    await appgenerator(appTree, options);
    const config = readProjectConfiguration(appTree, 'test');
    expect(config).toBeDefined();
  });

  it('should update poetry', async () => {
    appTree.root = `${__dirname}/testdata`
    await appgenerator(appTree, options);
    const data = appTree.read('pyproject.toml')
    const tomlParsed = toml.parse(data.toString())
    expect(tomlParsed['tool']['poetry']['scripts']).toHaveProperty('test')
    expect(tomlParsed['tool']['poetry']['scripts']['test']).toBe('test:main')
  })

  it('should update poetry with fastapi', async () => {
    appTree.root = `${__dirname}/testdata`
    options['fastapi'] = true
    await appgenerator(appTree, options);
    const data = appTree.read('pyproject.toml')
    const tomlParsed = toml.parse(data.toString())
    expect(tomlParsed['tool']['poetry']['dependencies']).toHaveProperty('fastapi')
    expect(tomlParsed['tool']['poetry']['dependencies']).toHaveProperty('uvicorn')
    expect(tomlParsed['tool']['poetry']['dependencies']).toHaveProperty('httpx')
  })

  it('should update poetry with websocket', async () => {
    appTree.root = `${__dirname}/testdata`
    options['websocket'] = true
    await appgenerator(appTree, options);
    const data = appTree.read('pyproject.toml')
    const tomlParsed = toml.parse(data.toString())
    expect(tomlParsed['tool']['poetry']['dependencies']).toHaveProperty('fastapi')
    expect(tomlParsed['tool']['poetry']['dependencies']).toHaveProperty('uvicorn')
    expect(tomlParsed['tool']['poetry']['dependencies']).toHaveProperty('httpx')
    expect(tomlParsed['tool']['poetry']['dependencies']).toHaveProperty('websockets')
  })
});
