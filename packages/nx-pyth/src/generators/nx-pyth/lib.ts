import { generateFiles, getWorkspaceLayout, names, offsetFromRoot, ProjectConfiguration, Tree, ProjectType, readJsonFile } from "@nrwl/devkit";
import { NxPythGeneratorSchema } from "./schema";
import * as path from 'path';

import { getPyProjectStringWithUpdatedPackages, getPyProjectStringWithUpdatedScripts, pyPackage, Scripts, getPyProjectStringWithUpdatedDependencies, Dependencies, getFastApiDependencies } from "./updatepy";

interface NormalizedSchema extends NxPythGeneratorSchema {
    projectName: string;
    projectRoot: string;
    projectDirectory: string;
    parsedTags: string[];
  }

export  function normalizeOptions(
    tree: Tree,
    options: NxPythGeneratorSchema,
    projectType: ProjectType
  ): NormalizedSchema {
    const name = names(options.name).fileName;
    const projectDirectory = options.directory
      ? `${names(options.directory).fileName}/${name}`
      : name;
    const projectName = projectDirectory.replace(new RegExp('/', 'g'), '-');
    let basePath: string
    if (projectType == "library") {
        basePath = getWorkspaceLayout(tree).libsDir
    } else {
        basePath = getWorkspaceLayout(tree).appsDir
    }
    const projectRoot = `${basePath}/${projectDirectory}`;
    const parsedTags = options.tags
      ? options.tags.split(',').map((s) => s.trim())
      : [];
  
    return {
      ...options,
      projectName,
      projectRoot,
      projectDirectory,
      parsedTags,
    };
}

type Names = ReturnType<typeof names>
type AppType = "basic" | "fastapi"

interface TemplateOptions extends NormalizedSchema, Names{
    offsetFromRoot: string,
    template: string,
    workspaceName: string
    appType: AppType
    websocket: boolean

}



function getTemplateOptions(tree: Tree, options: NormalizedSchema): TemplateOptions {
    const packageJson = readJsonFile(`${tree.root}/package.json`)

    const workspaceName = (packageJson['name'] as string).replace(/(\/\w*)/,"")
    let appType: AppType
    appType = "basic"
    appType = options.fastapi? "fastapi" : appType
    appType = options.websocket? "fastapi" : appType
    const websocket = options.websocket? true : false
    return  {
        ...options,
        ...names(options.name),
        offsetFromRoot: offsetFromRoot(options.projectRoot),
        workspaceName,
        template: '',
        appType,
        websocket,
      }
}



// addFiles(tree, normalizedOptions, "lib");
export function addFiles(tree: Tree, options: NormalizedSchema, projectType: ProjectType) {
    const templateOptions = getTemplateOptions(tree, options)
    let sourcePath: string
    if (projectType == "library") {
        sourcePath = path.join(__dirname, 'libfiles')
    } else {
        sourcePath = path.join(__dirname, 'appfiles')
    }
    if ( templateOptions.appType === 'fastapi' ) {
      const fastapiPath = path.join(__dirname, 'fastapifiles')
      generateFiles(
        tree,
        fastapiPath,
        options.projectRoot,
        templateOptions
      );
    }
    if ( templateOptions.websocket ) {
      const websocketPath = path.join(__dirname, 'websocketfiles')
      generateFiles(
        tree,
        websocketPath,
        options.projectRoot,
        templateOptions
      );
    }
    generateFiles(
      tree,
      sourcePath,
      options.projectRoot,
      templateOptions
    );
  }
export async function  updatePoetry(tree: Tree, options: NormalizedSchema, projectType: ProjectType){
    const templateOptions = getTemplateOptions(tree, options)
    if (!tree.exists("pyproject.toml")) {
        const sourcePath = path.join(__dirname, 'root-files')
        generateFiles(
            tree,
            sourcePath,
            ".",
            templateOptions
            );
    }
    let pyproject = tree.read("pyproject.toml")
    const pyPackage: pyPackage = {
        from: `${options.projectRoot}/src/`,
        include: options.projectName
    }
    let updatedPyprojectString = getPyProjectStringWithUpdatedPackages(pyproject, pyPackage)
    tree.write("pyproject.toml", updatedPyprojectString)
    if (projectType == "application") {
        pyproject = tree.read("pyproject.toml")
        const script: Scripts = {}
        script[options.projectName] = `${options.projectName}:main`
        updatedPyprojectString = getPyProjectStringWithUpdatedScripts(pyproject, script)
        if (options.fastapi || options.websocket)  {
          const fastApiDependenciesPath = path.join(__dirname, 'fastapi.toml')
          updatedPyprojectString = await getPyProjectStringWithUpdatedDependencies(updatedPyprojectString,fastApiDependenciesPath, options.websocket)
        }
        tree.write("pyproject.toml", updatedPyprojectString)
    }
}

export function generateProjectConfig(normalizedOptions: NormalizedSchema, projectType: ProjectType): ProjectConfiguration{
    let serve
    const name = normalizedOptions.name
    if (normalizedOptions.fastapi || normalizedOptions.websocket) {
      serve = {
        executor: "nx:run-commands",
        configurations: {
          dev: {
            command: `.venv/bin/uvicorn ${name}.config:get_app --reload --factory`
          },
          production: {
            command: `.venv/bin/${name}`
          }
        }
      }
    } else {
      serve = {
        executor: "nx:run-commands",
        options: {
          command: `.venv/bin/${name}`
        }
      }
    }
    return {
        root: normalizedOptions.projectRoot,
        projectType,
        sourceRoot: `${normalizedOptions.projectRoot}/src`, 
        targets: {
          lint: {
            executor: '@nxtemplate/nx-pyth:lint',
          },
          test: {
            executor: '@nxtemplate/nx-pyth:test',
          },
          serve
        },
        tags: normalizedOptions.parsedTags,
      };
}