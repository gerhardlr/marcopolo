import * as toml from 'toml';
import * as fs from 'fs/promises'
export type pyPackage = {include: string, from: string}
export type Scripts = Record<string, string>
export type Dependencies =Record<string, string> | Map<string, string>

function stringifyPackages(pyPackages:pyPackage[]): string{
    let stringifiedPackages = ""
    let index = pyPackages.length
    for (const pyPackage of pyPackages) {
        index -= 1
        if (index) {
            stringifiedPackages += `    { include = "${pyPackage.include}", from = "${pyPackage.from}" },\n`
        } else {
            stringifiedPackages += `    { include = "${pyPackage.include}", from = "${pyPackage.from}" }\n`
        }
    }
    return stringifiedPackages
}

function stringifyDependencies(dependencies:Map<string, string>): string{
    let stringifiedDependencies = ""
    for (let [key, value] of dependencies) {
        stringifiedDependencies += `${key} = "${value}"\n`
    }    
    return `${stringifiedDependencies}\n`
}

function stringifyScripts(scripts: Scripts): string{
    let stringifiedScripts = ""
    for (const property in scripts) {
        stringifiedScripts += `${property} = "${scripts[property]}"\n`
    }    
    return `${stringifiedScripts}\n`
}

export function getFastApiDependencies(buffer: Buffer, websocket?: boolean) {
    const fastApiString = buffer.toString()
    const fastApiObject = toml.parse(fastApiString)
    let dependencies = fastApiObject['dependencies'] as Dependencies
    if (websocket) {
        const websocketDependencies = fastApiObject['websocket'] as Dependencies
        dependencies = merge(dependencies,websocketDependencies)
    }
    return dependencies
  }

export function getPyProjectStringWithUpdatedPackages(buffer: Buffer, pyPackage:pyPackage) {
    const regex = /(?<=packages = \[\n)([^\]]*)/gm;
    const pyprojectString = buffer.toString()
    const pyprojectObject = toml.parse(pyprojectString)
    const currentPackages = pyprojectObject['tool']['poetry']['packages']
    const updatedPackages = [...currentPackages, ...[pyPackage]]
    const stringifiedPackages = stringifyPackages(updatedPackages)
    return pyprojectString.replace(regex,stringifiedPackages)
}

function mapToArray<V>(inputMap: Map<string,V>){
    const mapArray: [string, V][] = []
    inputMap.forEach((value, key) => {
        mapArray.push([key,value])
    }) 
    return mapArray
}

function merge<V>(objectA: Record<string, V> | Map<string,V>, objectB: Record<string, V> | Map<string, V>): Map<string,V> {

    let ArrayOfObjectA:[string, V][]
    if (objectA instanceof Map) {
        ArrayOfObjectA = mapToArray(objectA)
    } else {
        ArrayOfObjectA = Object.entries(objectA)
    }
    let ArrayOfObjectB:[string, V][]
    if (objectB instanceof Map) {
        ArrayOfObjectB = mapToArray(objectB)
    } else {
        ArrayOfObjectB = Object.entries(objectB)
    }
    return new Map([...ArrayOfObjectA, ...ArrayOfObjectB])

}

export async function getPyProjectStringWithUpdatedDependencies(buffer: Buffer | string, dependenciesPath: string, websocket?: boolean) {

    const file = await fs.readFile(dependenciesPath)
    const dependencies = getFastApiDependencies(file, websocket)
    const regex = /(?<=\[tool\.poetry\.dependencies\]\n)([^\[]*)/gm;
    let pyprojectString: string;
    if (typeof buffer != 'string') {
        pyprojectString = buffer.toString()
    } else {
        pyprojectString = buffer
    }
    const pyprojectObject = toml.parse(pyprojectString)
    let currentDependencies = pyprojectObject['tool']['poetry']['dependencies'] as Dependencies
    const updatedDependencies = merge(currentDependencies,dependencies)
    const stringifiedDependencies = stringifyDependencies(updatedDependencies)
    return pyprojectString.replace(regex,stringifiedDependencies)
}

export function getPyProjectStringWithUpdatedScripts(buffer: Buffer, script: Scripts) {
    const regex = /(?<=\[tool.poetry.scripts\]\n)([^[]*)/gms;
    const pyprojectString = buffer.toString()
    const pyprojectObject = toml.parse(pyprojectString)
    const currentScripts = pyprojectObject['tool']['poetry']['scripts']
    const updatedScripts = {...currentScripts, ...script}
    const stringifiedScripts = stringifyScripts(updatedScripts)
    return pyprojectString.replace(regex,stringifiedScripts)
}