import {
    addProjectConfiguration,
    formatFiles,
    Tree,
  } from '@nrwl/devkit';
  import { NxPythGeneratorSchema } from './schema';
  import { normalizeOptions, addFiles, updatePoetry, generateProjectConfig} from './lib'
import commandInShell from '../../executors/lib/utils';
  
  
  
  export default async function (tree: Tree, options: NxPythGeneratorSchema) {
    const normalizedOptions = normalizeOptions(tree, options, "application");
    const projectConfiguration = generateProjectConfig(normalizedOptions, "application")
    addProjectConfiguration(tree, normalizedOptions.projectName, projectConfiguration);
    addFiles(tree, normalizedOptions, "application");
    await updatePoetry(tree,normalizedOptions,"application")
    await formatFiles(tree);
    await commandInShell(`poetry install`)
  }