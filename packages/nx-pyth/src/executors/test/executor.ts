import { TestExecutorSchema } from './schema';
import commandInShell, { getPath, getProjectRoot } from '../lib/utils';
import { ExecutorContext } from '@nrwl/devkit';


export default async function runExecutor(options: TestExecutorSchema, context: ExecutorContext) {
  const projectRoot = getProjectRoot(context)
  const basePath = await getPath()
  return commandInShell(`${basePath}/bin/pytest ${projectRoot}/tests`)
}
