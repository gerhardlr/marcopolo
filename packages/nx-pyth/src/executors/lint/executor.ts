import { LintExecutorSchema } from './schema';
import commandInShell from '../lib/utils';
import { getProjectRoot, getPath } from '../lib/utils'
import { ExecutorContext } from '@nrwl/devkit';


export default async function runExecutor(options: LintExecutorSchema, context: ExecutorContext) {
  const projectRoot = getProjectRoot(context)
  const basePath = await getPath()
  return commandInShell(`${basePath}/bin/flake8 ${projectRoot}`)
}
