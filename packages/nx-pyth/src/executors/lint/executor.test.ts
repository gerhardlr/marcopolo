import { LintExecutorSchema } from './schema';
import executor from './executor';
import { getMockedContext } from '../lib/utils';


function mockFn(command, callback)  {
  callback(null, {stdout: "stdout"}, {stderr: "stderr"})
}
jest.mock('node:child_process', () => { return {exec: mockFn}})

const options: LintExecutorSchema = {};


describe('Build Executor', () => {
  const  context = getMockedContext()

  it('can run', async () => {
    const output = await executor(options, context);
    expect(output.success).toBe(true);
  });
});
