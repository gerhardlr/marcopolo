{{- range .Values.frontends }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $.Chart.Name }}-{{ .name }}
  labels:
    env: {{ $.Values.env.type }}
spec:
  replicas: {{ .replicaCount }}
  selector:
    matchLabels:
      {{- include "helpers.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- include  "helpers.podAnnotations" $ }}
      labels:
        {{- include "helpers.selectorLabels" . | nindent 8 }}
    spec:
      initContainers:
        - name: build
          image: {{ include "helpers.image" $ }} 
          imagePullPolicy: {{ include "helpers.pullPolicy" $ }} 
          command: ["/bin/sh","-c"]
          args: ["nx build $APP --baseHref=$HREF --outputPath=/build && mv /build/* /output/"]
          env:
            - name: APP
              value: {{ .name }}
            - name: HREF
              value: {{ include "helpers.path" $ }}/{{ .path }}/
          volumeMounts:
            - name: build
              mountPath: /output
      containers:
        - name: {{ .name }}
          image: nginx:latest 
          imagePullPolicy: IfNotPresent 
          volumeMounts:
            - name: build
              mountPath: /usr/share/nginx/html/
            - name: config
              mountPath: /etc/nginx/conf.d/default.conf
              subPath: default.conf
          ports:
            - name: http
              containerPort: 80
              protocol: TCP
      volumes:
        - name: build
          emptyDir: {}
        - name: config
          configMap:
            name: {{ $.Chart.Name }}-nginx-config
---
apiVersion: v1
kind: Service
metadata:
  name: {{ $.Chart.Name }}-{{ .name }}
  labels:
      env: {{ $.Values.env.type }}
spec:
  type: ClusterIP
  ports:
    - port: 80
      targetPort: http
      protocol: TCP
      name: http
  selector:
    {{- include "helpers.selectorLabels" . | nindent 4 }}
---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
    name: {{ $.Chart.Name }}-{{ .name }}
    labels:
      env: {{ $.Values.env.type }}
    annotations:
        nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
    ingressClassName: nginx
    rules:
    - http:
        paths:
        - path: {{ include "helpers.path" $ }}/{{ .name }}/?(.*)
          pathType: Prefix
          backend:
            service:
                name: {{ $.Chart.Name }}-{{ .name }}
                port:
                    number: 80
---
{{- end}}