import {  ExecutorContext, formatFiles } from "@nrwl/devkit";

//import { generateFiles, getWorkspaceLayout, names, offsetFromRoot, ProjectConfiguration, Tree, ProjectType, readJsonFile } from "@nrwl/devkit";
import { AddAppExecutorSchema } from './schema';
import  { getProjectRoot, getAppRoot } from './lib/utils';
import { addBackend, AppObject } from './editvalues';
import { updateDependencies } from './editproject';

export default async function runExecutor(options: AddAppExecutorSchema, context: ExecutorContext) {
  const projectRoot = getProjectRoot(context)
  const projectName = context.projectName
  const name = options.name
  const backendRoot = getAppRoot(name, context)
  console.log(`Adding ${backendRoot} to ${projectRoot}`)
  const backend: AppObject = {
    name,
    path: name,
    replicaCount: 1,
    port: 5000
  }
  await addBackend(backend, projectRoot, projectName)
  await updateDependencies(backend.name, projectRoot)
  return {
    success: true,
  };
}
