import { ExecutorContext, ProjectsConfigurations } from '@nrwl/devkit';
import * as fs from 'fs/promises'
import * as path from 'path';
import { buffer } from 'stream/consumers';
import * as YAML from 'yaml';


export function getProjectRoot(context: ExecutorContext){
  const projName = context.projectName
  const projects = context.workspace.projects
  return projects[projName].root
}

export function getAppRoot(appName: string, context: ExecutorContext){
    const projects = context.workspace.projects
    return projects[appName].root
}

export const mockedProperties = {
  testAppName: "myApp",
  deploymentName: "myDeployment"
}

export class MockedFolder {

  private project_json: any

  constructor(){
    this.project_json = getMockedContext().workspace.projects[mockedProperties.deploymentName]
  }

  private getDir(){
    const updated_path = `${mockedProperties.deploymentName}/${mockedProperties.deploymentName}/`
    const base = path.resolve("dist")
    return path.join(base,updated_path)
  }

  private getBaseDir(){
    const updated_path = `${mockedProperties.deploymentName}/`
    const base = path.resolve("dist")
    return path.join(base,updated_path)
  }

  public async getValuesFile() {
    const data = await fs.readFile(path.join(this.getDir(),"values.yaml"))
    return YAML.parse(data.toString())
  }

  public async getProjectJsonFile() {
    const data = await fs.readFile(path.join(this.getBaseDir(),"project.json"))
    return JSON.parse(data.toString())
  }

  public async setup(){
    const directory = this.getDir()
    await fs.mkdir(directory, {recursive:true})
    const valuesFile = "values.yaml"
    const src = path.join(__dirname,"..","testdata",valuesFile)
    await fs.copyFile(src,path.join(directory,valuesFile))
    const baseDirectory = this.getBaseDir()
    const projectFileName = path.join(baseDirectory,"project.json")
    const projectFileData = JSON.stringify(this.project_json)
    await fs.writeFile(projectFileName,projectFileData)
  }


  public async teardown() {
    const directory = this.getBaseDir()
    await fs.rmdir(directory,{recursive:true} )

  }
}

export function getMockedContext() {
  const workspace = {
    projects: {}
  } as ProjectsConfigurations
  const  context: ExecutorContext = { 
    targetName: "dummy",
    projectName: mockedProperties.deploymentName,
    workspace: workspace} as ExecutorContext
  context.workspace.projects = {
    myApp: {
        name: mockedProperties.testAppName,
        projectType: 'application',
        sourceRoot: `dist/${mockedProperties.testAppName}/src`,
        tags: [],
        root: `dist/${mockedProperties.testAppName}`,
        implicitDependencies: []
      },
      myDeployment: {
        name: mockedProperties.deploymentName,
        projectType: 'application',
        sourceRoot: `dist/${mockedProperties.deploymentName}/src`,
        tags: [],
        root: `dist/${mockedProperties.deploymentName}`,
        implicitDependencies: []
      }
  } 
  return context
}