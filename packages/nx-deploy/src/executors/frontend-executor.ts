import {  ExecutorContext, names } from "@nrwl/devkit";

//import { generateFiles, getWorkspaceLayout, names, offsetFromRoot, ProjectConfiguration, Tree, ProjectType, readJsonFile } from "@nrwl/devkit";
import { AddAppExecutorSchema } from './schema';
import  { getProjectRoot, getAppRoot } from './lib/utils';
import { addFrontend, AppObject } from './editvalues';
import { updateDependencies } from './editproject';

export default async function runExecutor(options: AddAppExecutorSchema, context: ExecutorContext) {
  const projectRoot = getProjectRoot(context)
  const projectName = context.projectName
  const name = options.name
  const frontendRoot = getAppRoot(name, context)
  console.log(`Adding ${frontendRoot} to ${projectRoot}`)
  const frontend: AppObject = {
    name,
    path: name,
    replicaCount: 1,
    port: 5000
  }
  await addFrontend(frontend, projectRoot, projectName)
  await updateDependencies(frontend.name, projectRoot)
  return {
    success: true,
  };
}
