import * as fs from 'fs/promises'
import * as path from 'path';
import * as Prettier from 'prettier';

export async function updateDependencies(dependency: string, root: string) {
    const filePath = path.join(root,"project.json")
    const data = await fs.readFile(filePath)
    const jsonData = JSON.parse(data.toString())
    const implicitDependencies = jsonData.implicitDependencies as string[]
    implicitDependencies.push(dependency)
    const updatedData = JSON.stringify(jsonData, null, 4)
    await fs.writeFile(filePath,updatedData)
}